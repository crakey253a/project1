﻿/*******************************************************************
 * - Game                 : Guess Number                           *
 * - Author               : Khai Danh                              *
 * - Date                 : 17/01/2021                             *
 * *****************************************************************/

 /* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include<stdbool.h>
#include<malloc.h>
#include<string.h>

char string_int[10];
 /* Define function, const ----------------------------------------------------*/
#define RANDOM (rand() % (10)) //random number 0 to 9
#define NUM_ELEMENTS 4
//debug
#define PRINT_RANDOM //print random numbers
//#define TEST_COUNT //print test numbers 

 /* Variables for arrary-------------------------------------------------------*/
int p[NUM_ELEMENTS]; //store numbers random
int q[NUM_ELEMENTS]; //store numbers user enter
static int n;
static char c;       //check keyboard user press
static char yes_no;       //check keyboard user press
bool win = false;
/* declare function ----------------------------------------------------------*/
void random_numbers();
void game_guide();
void play();
void check_win_replay();
void check_hit_blow();
int* convert_to_array(char* char_int);
bool check_not_char(int* b);
bool check_duplicate(char* c);
bool find_index(int a[], int num_elements, int value);


/*
  - check duplicate value
   ---------------------------------------------------------------------------*/
bool find_index(int a[], int num_elements, int value)
{
	for (int i = 0; i < num_elements; i++)
	{
		if (a[i] == value)
		{
			return true;
		}
	}
	return false;
}

/*
  - check array user enter
   ---------------------------------------------------------------------------*/
bool check_duplicate(char *c) //array user enter
{
	int temp, n;
	bool tf = true;
    n = strlen(c);
	if (n!=4)
	{
		tf = false;
	}
	for (int i = 0; i < strlen(c); i++)
	{
		temp = *(c + i);
		for (int j = 0; j < strlen(c); j++)
		{
			if ((*(c + i) == *(c + j)) && (i != j))
			{
				tf = false;
			}
			if (i == j)
			{
				break;
			}
		}

	}
	return tf;
}
/*
  - Generate random number for array
   ---------------------------------------------------------------------------*/
void random_numbers() {
	int i = 0;
	p[0] = RANDOM;
	bool check_dif = true;
	while (i < (NUM_ELEMENTS - 1)) {
		int c = RANDOM;
		for (int j = i; j >= 0; j--) {
			if (c == p[j]) {
				check_dif = false;
				break;
			}
		}
		if (check_dif == true) {
			p[i + 1] = c;
			i++;
		}
		else {
			i--;
			check_dif = true;
		}
	}
}

/*
  - start game, game guide
   ---------------------------------------------------------------------------*/
void game_guide() {
	static char ready;
	printf("1. Press p to play game\n");
	printf("2. Enter the number you guess\n");
	printf("3. Use HIT and BLOW to solve game\n");
	printf("4. If you enter the correct number, you win!\n");
	printf(" Are you sure!");
	while ((ready != '\n')) {
		ready = getchar();
	}
	c = 'p';
	printf("Let's start!\n");
}
/*
  - check hit and blow value
   ---------------------------------------------------------------------------*/
void check_hit_blow(int *c) {
	int count_hit = 0;
	int count_blow = 0;
	//check hit;
	for (int i = 0; i < NUM_ELEMENTS; ++i) {
		if ( *(c + i) == p[i] ) {
			count_hit++;
		}
	}
	if (count_hit == 4) {
		win = true;
	}
	//check blow
	for (int i = 0; i < NUM_ELEMENTS; i++) {
		for (int j = 0; j < NUM_ELEMENTS; j++) {
			if ( *(c + j) == p[i] && j != i) {
				count_blow++;
			}
		}
	}
	printf("  Hit = %d\n", count_hit);
	printf("  Blow = %d\n", count_blow);
}
/*
  - check win status, choose replay
   ---------------------------------------------------------------------------*/
void check_win_replay() {
	if (win == true) {
		printf("Correct number: \n");
		for (int i = 0; i < NUM_ELEMENTS; ++i) {
			printf("%d", p[i]);
		}
		printf("*************************************\n");
		printf("*            YOU WIN                *\n");
		printf("*        CONGRATULATIONS!           *\n");
		printf("*************************************\n");
		printf("Do you want to play again. Yes/No: Press y/n \n");

		do {
			yes_no = getchar();
		} while ((yes_no != 'y') && (yes_no != 'n'));

		if (yes_no == 'y') {
			c = 'p';
			random_numbers();
			printf("* YES *\n");
		}
		else {
			c = 'e';
			printf("* NO *\n");
		}
	}
	win = false;
}

/*
  - play game
   ---------------------------------------------------------------------------*/
void play() {
	while (win != true)
	{
		char* number_ptr;
		printf("Enter number\n");
		scanf_s("%s", string_int, 10);
		number_ptr = convert_to_array(string_int);
		while ((check_not_char(number_ptr) != true) | (check_duplicate(string_int) != true)) {
			printf("Enter number again\n");
			scanf_s("%s", string_int, 10);
			number_ptr = convert_to_array(string_int);
		}
#ifdef PRINT_RANDOM
		for (int i = 0; i < NUM_ELEMENTS; ++i) {
			printf("%d", p[i]);
		}
		printf("\n");
#endif // PRINT_RANDOM
		check_hit_blow(number_ptr);
		check_win_replay();
		free(number_ptr);
	}
}

int* convert_to_array(char* char_int)
{
	int i, len;
	int* pointer_array;
	len = strlen(char_int);
	pointer_array = (int*)malloc(len * sizeof(int));
	for (i = 0; i < len; i++)
	{
		pointer_array[i] = char_int[i] - 48;
	}
	return pointer_array;
}
bool check_not_char(int* b)
{
	for (int i = 0; i < strlen(string_int); i++) {
		if (*(b + i) > 9)
		{
			return false;
		}
	}
	return true;
}

int main() {
	srand((int)time(0));
	random_numbers();

	printf("*************************************\n");
	printf("**           GUESS NUMBER          **\n");
	printf("**    Press p to PLAY              **\n");
	printf("**    Press g to see GAME GUIDES   **\n");
	printf("**    Press e to EXIT              **\n");
	printf("**                                 **\n");
	printf("*************************************\n");
	//Print test case
#ifdef TEST_COUNT
	p[0] = 8;
	p[1] = 1;
	p[2] = 7;
	p[3] = 4;
#endif // TEST_COUNT
	
	
	while (c != 'e') {
		c = getchar();
		switch (c) {
		case 'g':
			game_guide();
			break;
		case 'p':
			play();
			break;
		default:
			printf("Press key to Play\n");
			break;
		}
	}

}



